var getImgDir = function(elmIMG) {
  return elmIMG.getAttribute("img-dir");
};

var setImgSrcImmediately = function(elmIMG) {
  var t = getImgDir(elmIMG) + "/" + elmIMG.title; 
  if (title2base64 && title2base64[t]) {
    elmIMG.src = title2base64[t];
  } else {
    elmIMG.src = t;
  }
}

var setImgSrc = function(elmIMG, i, j) {
  var t = getImgDir(elmIMG) + "/" + elmIMG.title; 
  var d = (i * 20) + (j * 5);
  // console.log("enque " + t + " @ " + d.toString(10) + "msec later");
  elmIMG.h2to = setTimeout(function(){
    // console.log("show " + t);
    if (title2base64 && title2base64[t]) {
      elmIMG.src = title2base64[t];
    } else {
      elmIMG.src = t;
    }
  }, d);
}


var getTRsFromTBody = function(visibleOnly) {
  var trs = new Array();
  var _tbodys = document.getElementsByTagName("tbody");
  for (var i = 0; i < _tbodys.length; i += 1) {
    var _trs = _tbodys[i].getElementsByTagName("tr");
    for (var j = 0; j < _trs.length; j += 1) {
      if (!visibleOnly || _trs[j].offsetHeight) {
        trs.push(_trs[j]);
      }
    }
  }
  return trs;
};

var trs;
var firstTRVisible;
var trOffsetTop;
var trOffsetHeight;
var rowOffsets = new Array();

var resetTRS = function() {
  trs = getTRsFromTBody(true);
  firstTRVisible = trs[0];
  trOffsetTop = firstTRVisible.offsetTop;
  trOffsetHeight = firstTRVisible.offsetHeight;
  rowOffsets.length = 0;
};
resetTRS();
$("div.content").on("contentmodified", function(){
  resetTRS();
});

var scrollStopEvent = new $.Event("scrollstop");
function scrollStopEventTrigger() {
  var scrollStopTimer = null;
  if (scrollStopTimer) {
    clearTimeout(scrollStopTimer);
  } 
  scrollStopTimer = setTimeout(function(){$(window).trigger(scrollStopEvent)}, 500);
}
$(window).on("scroll", scrollStopEventTrigger);


var recordRowOffsets = function() {
  for (var i = 0; i < trs.length; i += 1) {
    rowOffsets.push(trs[i].offsetTop);
  }
}
var updateImgSrc = function(timeOut){
  var pos = document.body.scrollTop;
  var i0;
  var i1;
  if (trs.length != rowOffsets.length) {
    recordRowOffsets();
  }
  if (rowOffsets.length == 0) {
    i0 = Math.floor((pos - trOffsetTop - window.innerHeight )/trOffsetHeight) - 2;
    i1 = Math.min(trs.length, Math.ceil((pos + window.innerHeight + window.innerHeight - trOffsetTop)/trOffsetHeight) + 1);
  } else {
    for (i0 = 0; i0 < rowOffsets.length; i0 += 1) {
      if ((pos - trOffsetTop) < rowOffsets[i0]) {
        if (0 < i0) { 
          i0 = i0 - 1;
        }
        for (i1 = i0; i1 < rowOffsets.length; i1 += 1) {
          if (pos + 2 * (window.innerHeight) - trOffsetTop < rowOffsets[i1]) {
            break;
          }
        }
        break;
      }
    }
  }
  if (i1 == undefined || i1 == rowOffsets.length) {
    i1 = Math.min(i0 + 50, rowOffsets.length); 
  }
  if (i1 == rowOffsets.length) {
    i1 = i1 - 1;
  }
  console.log(trs[i0].getElementsByClassName("seq")[0].textContent + " - " + trs[i1].getElementsByClassName("seq")[0].textContent);
  for (var i = i0; i <= i1; i += 1) {
    if (i < 0) {
      continue;
    }
    var imgs = trs[i].getElementsByTagName("img");
    for (var j = 0; j < imgs.length; j += 1) {
      if (!imgs[j].src) {
        setImgSrc(imgs[j], i - i0, j);
      }
    }
  }

  // code to reset src attribute of out-of-window images.
  if ( false ) {
    for (var i = 0; trOffsetTop + (i + 1) * trOffsetHeight < pos && i < trs.length; i += 1) {
      var imgs = trs[i].getElementsByTagName("img");
      for (var j = 0; j < imgs.length; j += 1) {
        if (imgs[j].src) {
          imgs[j].src = null;
        }
      }
    }
    for (var i = i1; i < trs.length; i += 1) {
      var imgs = trs[i].getElementsByTagName("img");
      for (var j = 0; j < imgs.length; j += 1) {
        if (imgs[j].src) {
          imgs[j].src = null;
        }
      }
    }
  }
};

$(window).on("scrollstop", function(){updateImgSrc(300);});
$(window).ready(function(){
  recordRowOffsets();
  updateImgSrc(300);
});
$(window).on("contentmodified", function(){
  resetTRS();
  recordRowOffsets();
  updateImgSrc(300);
});

var loadAllImgs = function() {
  $("img:visible").each(function(){setImgSrcImmediately(this);});
};
