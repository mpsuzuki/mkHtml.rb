#!/usr/bin/env ruby1.9
# encoding: euc-jp

Encoding.default_internal = "utf-8"
Encoding.default_external = "utf-8"

require "zlib"
require "nokogiri"

STDERR.printf("*** load gnumeric document...")
xmlIO = Zlib::GzipReader.new(STDIN)
xmlObj = Nokogiri::XML::Document.parse(xmlIO.read)
xmlIO.close
STDERR.printf("ok\n")

tsv = Array.new
STDERR.printf("*** loading cells...")
xmlObj.css("gnm|Sheets > gnm|Sheet > gnm|Cells").each do |gnmCells|
  gnmCells.element_children().each do |elm|
    next if (elm.name != "Cell")
    gnmCell = elm
    rowIdx = gnmCell["Row"].to_i
    colIdx = gnmCell["Col"].to_i
    tsv[rowIdx] = Array.new if (tsv[rowIdx] == nil)
    tsv[rowIdx][colIdx] = gnmCell.text()
  end
end
STDERR.printf("ok\n")

tsv.each do |row|
  puts row.join("\t")
end
