#!/usr/bin/env ruby1.9

imgDirs = ARGV.select{|a| File.directory?(a)}
Opts = {
  "dot-list-default" => ".list"
}
ARGV.select{|a| !File.directory?(a)}.each do |a|
  if (a =~ /^--.*=.*/)
    k,v = a.gsub(/^--/, "").split("=",2)
    Opts[k] = v
  elsif (a =~ /^--.*/)
    k = a.gsub(/^--/, "")
    Opts[k] = true
  end
end

imgDir2list = Hash.new
imgDirs.each do |imgDir|
  begin
    f = File.open(imgDir + "/" + Opts["dot-list"], "r")
  rescue => err
    begin
      f = File.open(imgDir + "/" + Opts["dot-list-default"], "r")
    rescue => err2
    end
  end

  if (f)
    imgDir2list[imgDir] = f.readlines.collect{|l| l.chomp.split(/[\t#]/, 2).first}
  else
    imgDir2list[imgDir] = Dir.glob(imgDir + "/*.[GPgp][INin][FGfg]").sort
  end
end

# p imgDir2list

# --

i = 0
while (1)
  if (imgDirs.none?{|imgDir| imgDir2list[imgDir].length > i})
    break
  end
  toks = Array.new
  # toks << ("%05d" % i)
  imgDirs.each do |imgDir|
    if (imgDir2list[imgDir][i])
      toks << (imgDir + "/" + imgDir2list[imgDir][i])
    else
      # toks << (imgDir + "/")
      toks << ""
    end
  end
  puts toks.join("\t")
  i += 1
end


