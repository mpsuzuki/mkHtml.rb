var procTap = function(evt) {
  var jqTR = $(evt.currentTarget);
  var jqTD = $(evt.target).parents("td");
  var jqTH = $(evt.target).parents("th");

  var newComment = "x";
  if (jqTD.length > 0) {
    var classes = jqTD.get(0).classList;
    for (var i = 0; i < classes.length; i += 1) {
      var t = classes[i];
      if (t.indexOf("img-") == 0) {
        newComment = t.split("img-", 2).pop();
        break;
      }
    }
  }

  var divComment = jqTR.find("div.comment");
  if (!divComment.is(":visible")) {
    jqTR.toggleClass("with-comment");
    return false;
  }

  var oldComment = divComment.text();
  if (oldComment.length == 0) {
    divComment.append(newComment); 
    jqTR.addClass("with-comment");
  } else
  if (jqTH.length > 0 && newComment == "x") {
    divComment.empty();
    jqTR.removeClass("with-comment");
  } else
  if (oldComment.indexOf(newComment) < 0) {
    divComment.append(", " + newComment); 
    jqTR.addClass("with-comment");
  } else {
    return false;
  }
};


$("tbody > tr").on("tap", procTap);
$("tbody > tr").on("click", procTap);
$("tbody > tr").on("dblclick", procTap);
