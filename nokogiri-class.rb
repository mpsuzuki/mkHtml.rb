class Nokogiri::XML::Element
  def add_class(cls)
    if (self["class"] == nil)
      self["class"] = [cls].flatten.join(" ")
    else
      self["class"] = (self["class"].split(/\s+/) | [cls]).flatten.join(" ")
    end
    return self
  end

  def remove_class(cls)
    if (self["class"] != nil)
      self["class"] = (self["class"].split(/\s+/) - [cls].flatten).join(" ")
    end
    return self
  end

  def has_class(cls)
    if (self["class"] == nil)
      return false
    else
      return self["class"].split(/\s+/).any?{|c| c.downcase == cls.downcase}
    end
  end

  def get_classes()
    return self["class"].split(/\s+/)
  end
end
