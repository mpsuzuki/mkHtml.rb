#!/usr/bin/env ruby1.9
# encoding: euc-jp

require "json"
require "base64"
require "nokogiri"

class Nokogiri::XML::Element
  def add_class(cls)
    if (self["class"] == nil)
      self["class"] = [cls].flatten.join(" ")
    else
      self["class"] = (self["class"].split(/\s+/) + [cls]).flatten.join(" ")
    end
    return self
  end

  def remove_class(cls)
    if (self["class"] != nil)
      self["class"] = (self["class"].split(/\s+/) - [cls].flatten).join(" ")
    end
    return self
  end

  def has_class(cls)
    if (self["class"] == nil)
      return false
    else
      return self["class"].split(/\s+/).any?{|c| c.downcase == cls.downcase}
    end
  end
end


require "RMagick"

imgDirs = ARGV.select{|a| File.directory?(a)}
Opts = {
  "xuan-index-to-type" => {},
  "dot-list-default" => ".list",
  "split-per" => 1000,
  "row-height" => 50
}
ARGV.select{|a| !File.directory?(a)}.each do |a|
  if (a =~ /--output-html-prefix=/)
    Opts["output-html-prefix"] = a.split("=")[1..-1].join("=")
  elsif (a =~ /--split-per=/)
    Opts["split-per"] = a.split("=")[1..-1].join("=").to_i
  elsif (a =~ /--row-height=/)
    Opts["row-height"] = a.split("=")[1..-1].join("=").to_i
  elsif (a =~ /--show-source-info-only/)
    Opts["show-source-info-only"] = a.split("=", 2).last.split(",").collect{|v|
      if (v =~ /^[0-9]+$/)
        v.to_i
      else
        v
      end
    }
  elsif (a =~ /^--.*=.*/)
    k,v = a.gsub(/^--/, "").split("=",2)
    Opts[k] = v
  elsif (a =~ /^--.*/)
    k = a.gsub(/^--/, "")
    Opts[k] = true
  end
end

Opts["report-missing-source-info"] = Opts["report-missing-source-info"].split(",")

# ------------------------------------------------
if (Opts["char-type-json"])
  f = File.open(Opts["char-type-json"], "r")
  Opts["xuan-index-to-type"] = JSON.parse(f.read)
  f.close
end

# ------------------------------------------------
if (Opts["swucs-json"])
  f = File.open(Opts["swucs-json"], "r")
  Opts["swucs-json"] = JSON.parse(f.read.encode("utf-8", "utf-8"))
  f.close
end

# ------------------------------------------------
# prep markArr
markArr = Array.new
if (Opts["mark-list"])
  ml = File.open(Opts["mark-list"], "r")
  while ($_ = ml.gets)
    markArr[$_.to_i] = true
  end
  ml.close
end

# ------------------------------------------------
# prep bn2fp, bn2imgDir

bn2fp = Hash.new
bn2imgDir = Hash.new
imgDir2imgs = Hash.new
imgDir2basenames = Hash.new
imgDirs.each do |imgDir|
  imgDir2imgs[imgDir] = nil

  begin
    f = File.open(imgDir + "/" + Opts["dot-list"], "r")
  rescue => err1
    begin
      f = File.open(imgDir + "/" + Opts["dot-list-default"], "r")
    rescue => err2
      f = nil
    end
  end
  if (!f)
    imgDir2imgs[imgDir] = (Dir.glob(imgDir + "/*.png") + Dir.glob(imgDir + "/*.gif")).sort
  else
    imgDir2imgs[imgDir] = f.readlines().collect{|t|
      t = t.chomp.split(/[#\t]/, 2).first
      if (t)
        imgDir + "/" + t
      else
        imgDir + "/"
      end
    }
    f.close
  end

  imgDir2basenames[imgDir] = imgDir2imgs[imgDir].collect{|v| File.basename(v)}
  imgDir2imgs[imgDir].each do |fp|
    bn = File.basename(fp)
    dn = File.dirname(fp)
    bn2fp[bn] = fp
    bn2imgDir[bn] = dn
  end
end

# ------------------------------------------------
if (Opts["swucs-json"])
  img2ucss = {
    "iwasaki" => {},
    "dyc" => {},
    "qjz" => {}
  }

  Opts["swucs-json"].keys.each do |ucs|
    hs = Opts["swucs-json"][ucs]
    img2ucss.keys.each do |book|
      next if (!hs.include?(book))
      hs[book].each do |ahs|
        next if (!ahs.include?("img"))
        img = ahs["img"]
        img2ucss[book][img] = Array.new  if (img2ucss[book][img] == nil)
        img2ucss[book][img] << ucs
      end
    end
  end

  ([ "img-Iwasaki", "img-DYC", "img-XZ-QJZ" ] & imgDirs).each do |imgDir|
    book = imgDir.split("-").last.downcase
    bookToUcss = imgDir.downcase + "-to-ucss"
    Opts[bookToUcss] = Hash.new

    imgsForUcss = img2ucss[book].keys.sort
    imgsInDir = imgDir2basenames[imgDir].sort

    # p imgsForUcss
    # p imgsInDir
    while (imgsForUcss.length > 0 && imgsInDir.length > 0)
      imgForUcss = imgsForUcss.shift
      i = imgsInDir.index{|bn| bn.include?(imgForUcss)}
      # p [imgForUcss, i]
      if (i != nil)
        imgInDir = imgsInDir[i]
        imgsInDir.delete_at(i)
        Opts[bookToUcss][imgInDir] = img2ucss[book][imgForUcss]
      end
    end
    # p Opts[bookToUcss]
  end
end
# ------------------------------------------------
imgDir2formats = Hash.new
imgDirs.each do |imgDir|
  if (File.exist?(imgDir + "/.format"))
    hs = Hash.new
    f = File.open(imgDir + "/.format", "r")
    while (f.gets)
      if ($_ =~ /^#\s*encoding[:\s]+/)
        e = $_.chomp.split(/encoding[:\s]+/, 2).last
        f.set_encoding(e, "utf-8")
        next
      end
      g, k, v = $_.chomp.split(/\s*:\s*/, 3)
      if (!hs.include?(g))
        hs[g] = Hash.new
      end
      hs[g][k] = v
    end
    f.close
    imgDir2formats[imgDir] = hs
  end
end
# p imgDir2formats

# ------------------------------------------------
imgDir2pageInfos = Hash.new
imgDirs.each do |imgDir|
  if (File.exist?(imgDir + "/.page.json"))
    f = File.open(imgDir + "/.page.json", "r")
    imgDir2pageInfos[imgDir] = JSON.parse(f.read)
    f.close
  end
end

# ------------------------------------------------

class Metrix < Hash
  def setW(w)
    self["w"] = w
    return self
  end

  def setH(h)
    self["h"] = h
    return self
  end

  def width
    return self["w"]
  end

  def height
    return self["h"]
  end
end

glyphMetrixCache = Hash.new
begin
  f = File.open(Opts["glyph-metrics-cache-json"], "r")
  _glyphMetrixCache = JSON.parse(f.read)
  _glyphMetrixCache.each do |k, v|
    glyphMetrixCache[k] = Metrix.new.setW(v["w"]).setH(v["h"])
  end
  f.close
rescue => err
  Opts["glyph-metrics-cache-has-new-info"] = true
end

# ------------------------------------------------
# prep maxHeights 

maxHeights = Hash.new
imgDirs.each do |imgDir|
  maxHeights[imgDir] = 0
end

imgDirs.each do |imgDir|
  heights = Array.new
  imgDir2imgs[imgDir].each do |fp|
    bn = File.basename(fp)
    height = 0
    next if (!File.file?(fp))

    if (glyphMetrixCache.include?(fp))
      width = glyphMetrixCache[fp].width
      height = glyphMetrixCache[fp].height
    else
      ping = Magick::ImageList.new.ping(fp)
      width = ping.columns
      height = ping.rows
      glyphMetrixCache[fp] = Metrix.new.setW(width).setH(height)
      Opts["glyph-metrics-cache-has-new-info"] = true
      ping.destroy!
    end
    heights << height
  end
  maxHeights[imgDir] = heights.max
end

# ------------------------------------------------

htmlDocSkel = <<EOS
<html>
  <head>
    <style>
      <!--
      table {
        border-collapse: collapse;
      }
      tr.hidden {
        display: none;
      }
      tr.marked {
        background: pink;
      }
      th, td {
        border: 1px solid black;
      }
      td.image {
        text-align: center;
        height: #row-height#;
      }
      td.glyphInfo {
        text-align: left;
        font-size: 8pt;
      }
      span.glyphInfo {
        word-break: keep-all;
        white-space: nowrap;
        overflow-x: hidden;
      }
      div.comment {
        width: 200px;
      }
      thead > tr > th {
        font-size: 12px;
      }
EOS


imgDirs.each do |imgDir|
  hs = imgDir2formats[imgDir]
  next if (hs == nil)
  if (hs.include?("css") && hs["css"].include?("background-color"))
    c = imgDir2formats[imgDir]["css"]["background-color"]
    htmlDocSkel += sprintf("th.%s { background-color: %s; }\n", imgDir.gsub("/", ""), c)
    htmlDocSkel += sprintf("td.%s { background-color: %s; }\n", imgDir.gsub("/", ""), c)
  end
  if (hs.include?("image") && hs["image"].include?("filter"))
    s = imgDir2formats[imgDir]["image"]["filter"]
    htmlDocSkel += sprintf("td.%s > img { filter: %s; -webkit-filter: %s; }\n", imgDir.gsub("/", ""), s)
  end
end

htmlDocSkel += <<EOS
      -->
    </style>
    <link href="jq-switch-text.css" rel="stylesheet" type="text/css">
    <link href="extra.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="jquery.min.js"></script>
    <script type="text/javascript" src="jquery.appear.js"></script>
    <script type="text/javascript" src="autoload-imgs.js"></script>
  </head>

  <body>
    <div class="menu navigator navigator-top" style="z-index:10;">
      <div class="menu menu_right" style="text-align: right;">
        <select class="show_hide">
          <option value="nop" selected>Show/Hide</option>
          <option value="showRowsAll">Show All</option>
          <option value="hideRowsWithComments">Hide Rows w/ Comments</option>
          <option value="hideRowsWithoutComments">Hide Rows w/o Comments</option>
          <option value="hideRowsWithUniqueImgs">Hide Rows w/ Uniq. Imgs</option>
          <option value="hideForPrint">Hide for Print</option>
        </select>
        <select class="stat_io">
          <option value="nop" selected>Save/Load</option>
          <option value="load">Load</option>
          <option value="save">Save</option>
        </select>
        <a class="hrefToNoteJson" style="display: none;"></a>
        <input class="fileToChooseJson" type="file" multiple style="width: 95px; display:none;">
      </div>
    </div>

    <form style='display:none'><textarea></textarea></form>

    <div class="content"></div>

    <div class="menu navigator navigator-bottom" style="z-index:10;">
      <div class="menu menu_right" style="text-align: right;">
        <select class="show_hide">
          <option value="nop" selected>Show/Hide</option>
          <option value="showRowsAll">Show All</option>
          <option value="hideRowsWithComments">Hide Rows w/ Comments</option>
          <option value="hideRowsWithoutComments">Hide Rows w/o Comments</option>
          <option value="hideRowsWithUniqueImgs">Hide Rows w/ Uniq. Imgs</option>
        </select>
        <select class="stat_io">
          <option value="nop" selected>Save/Load</option>
          <option value="load">Load</option>
          <option value="save">Save</option>
        </select>
        <a class="hrefToNoteJson" style="display: none;"></a>
        <input class="fileToChooseJson" type="file" multiple style="width: 95px; display:none;">
      </div>
    </div>


    <script type="text/javascript" src="lazyimg.js"></script>
    <script type="text/javascript" src="click-mark.js"></script>
  </body>
</html>
EOS

htmlDocSkel = htmlDocSkel.gsub("#row-height#", (Opts["row-height"] + 4).to_s)

# --------------------------------------------------

def startNewHtml(htmlDocSkel, htmlIndex, linkToPrev, linkToNext)
  htmlObj = Nokogiri::HTML::Document.parse(htmlDocSkel)
  elmBody = htmlObj.css("body").first
  elmContent = htmlObj.root.css("div.content").first

  elmLinkToPrevAndNext = Nokogiri::XML::Element.new("div", htmlObj)
  elmContent << elmLinkToPrevAndNext
  elmLinkToPrevAndNext.add_class("link-to-prev-and-next");

  if (linkToPrev)
    elmA = Nokogiri::XML::Element.new("a", htmlObj)
    elmLinkToPrevAndNext << elmA
    elmA << "prev"
    elmA["href"] = Opts["output-html-prefix"] + sprintf("%02d", (htmlIndex-1)) + ".html"
  end

  elmLinkToPrevAndNext << "&nbsp;-&nbsp;"

  if (linkToNext)
    elmA = Nokogiri::XML::Element.new("a", htmlObj)
    elmLinkToPrevAndNext << elmA
    elmA << "next"
    elmA["href"] = Opts["output-html-prefix"] + sprintf("%02d", (htmlIndex+1)) + ".html"
  end

  elmTable = Nokogiri::XML::Element.new("table", htmlObj)
  elmContent << elmTable

  elmTBody = Nokogiri::XML::Element.new("tbody", htmlObj)
  elmTable << elmTBody
  return [htmlObj, elmBody, elmTable]
end

def closeHtml(htmlObj, elmBody, htmlIndex, linkToPrev, linkToNext)
  elmA = Nokogiri::XML::Element.new("a", htmlObj)
  elmContent = htmlObj.root.css("div.content").first

  if (Opts.include?("postproc-remove-images"))
    Opts["postproc-remove-images"].split(",").each do |imgDir|
      p imgDir
      htmlObj.root.css("thead > tr > th." + imgDir).each do |elmTH|
        elmTH.remove()
      end
      htmlObj.root.css("tbody > tr > td." + imgDir).each do |elmTD|
        elmTD.remove()
      end
    end
  end

  elmLinkToPrevAndNext = Nokogiri::XML::Element.new("div", htmlObj)
  elmContent << elmLinkToPrevAndNext
  elmLinkToPrevAndNext.add_class("link-to-prev-and-next");

  if (linkToPrev)
    elmLinkToPrevAndNext << elmA
    elmA << "prev"
    elmA["href"] = Opts["output-html-prefix"] + sprintf("%02d", (htmlIndex-1)) + ".html"
  end

  elmLinkToPrevAndNext << "&nbsp;-&nbsp;"

  if (linkToNext)
    elmA = Nokogiri::XML::Element.new("a", htmlObj)
    elmLinkToPrevAndNext << elmA
    elmA << "next"
    elmA["href"] = Opts["output-html-prefix"] + sprintf("%02d", (htmlIndex+1)) + ".html"
  end

  elmScript = Nokogiri::XML::Element.new("script", htmlObj)
  elmBody << elmScript
  elmScript["type"] = "text/javascript"
  elmScript["src"]  = "jq-switch-text.js"

  ohp = Opts["output-html-prefix"] + sprintf("%02d", htmlIndex) + ".html"
  ohh = File.open(ohp, "w+")
  STDERR.puts ohp
  # ohh.puts(htmlObj.to_s)
  ohh.puts(htmlObj.to_xml(:encoding => "utf-8"))
  ohh.close
  htmlObj = nil
  htmlIndex += 1
  return [htmlObj, htmlIndex]
end

# --------------------------------------------------
imgDirForVolumeSep = imgDirs.select{|d| imgDir2imgs[d].first =~ /[_-][vV][0-9][0-9]/}.first
if (imgDirForVolumeSep == nil)
  imgDirForVolumeSep = imgDirs.select{|d| imgDir2imgs[d].first =~ /[_-][vV][oO][lL][0-9][0-9]/}.first
end
imgDirForRowIndex = imgDirs.sort_by{|d| imgDir2imgs[d].length}.last
# p [ imgDirForVolumeSep, imgDirForRowIndex ]

# --------------------------------------------------

htmlObj = nil
elmBody = nil
elmTable = nil

htmlIndex = 1
# imgDir2imgs[imgDirs.first].each_index do |i|
volLast = nil
seqs = {
  "Xuan" => 0,
  "Kai" => 0,
  "Duan" => 0,
  "Mao" => 0,
  "Kai_bug" => 0,
  "unknown" => 0
}
imgDir2imgs[imgDirForRowIndex].each_index do |i|
  if (!htmlObj)
    htmlObj, elmBody, elmTable, = startNewHtml(htmlDocSkel, htmlIndex, (htmlIndex > 1), (htmlIndex < 14))
  end

  elmTBODY = htmlObj.css("tbody").last

  elmTR = Nokogiri::XML::Element.new("tr", htmlObj)
  elmTBODY << elmTR

  elmTH = Nokogiri::XML::Element.new("th", htmlObj)
  elmTR << elmTH
  elmTH.add_class("seq")
  elmA = Nokogiri::XML::Element.new("a", htmlObj)
  elmTH << elmA
  elmA_seq = elmA


  elmTD = Nokogiri::XML::Element.new("td", htmlObj)
  elmTD.add_class("glyphInfo")
  elmTR << elmTD
  anyMissing = false
  imgDirs.each_with_index do |imgDir,j|
    next if (Opts["show-source-info-only"] && !Opts["show-source-info-only"].include?(j) && !Opts["show-source-info-only"].include?(imgDir))
    bn = File.basename(imgDir2imgs[imgDir][i])
    next if (!Opts["report-missing-source-info"].include?(imgDir) && (!imgDir2pageInfos.include?(imgDir) || !imgDir2pageInfos[imgDir].include?(bn)))
    # elmTD << Nokogiri::XML::Element.new("br", htmlObj) if (elmTD.children.length > 0)

    span_clazz = ["glyphInfo", imgDir]
    pageLocationAscii = imgDir2pageInfos[imgDir][bn]
    if (pageLocationAscii == nil)
      if (!anyMissing)
        anyMissing = true
        pageLocationAscii = "missing"
        pageLocationUtf8 = pageLocationAscii.gsub("missing", "��".encode("utf-8"))
        span_clazz << "missing"
      end
    else
      toks = pageLocationAscii.gsub("missing", "��".encode("utf-8")).split(".")
      if (!bn.include?("wg2n4688"))
        toks[0] = toks[0].gsub("v", "��".encode("utf-8")).gsub("a", "��".encode("utf-8")).gsub("b", "��".encode("utf-8"))
      end
      toks[1] = toks[1].gsub(/^l/, "��".encode("utf-8")).gsub("r", "��".encode("utf-8")).gsub("l", "��".encode("utf-8"))
      # toks[1] = toks[1].gsub(/^l/, "").gsub("r", "a").gsub("l", "b")
      if (toks[2] =~ /^l[0-9]{2}/)
        toks[2] = toks[2].gsub(/^l/, "��".encode("utf-8"))
      end
      if (toks.last =~ /^g[0-9]+$/ && imgDir2formats[imgDir].include?("gid") && imgDir2formats[imgDir]["gid"]["offset"] != nil)
        gid = sprintf("g%02d", toks.last[1..-1].to_i + imgDir2formats[imgDir]["gid"]["offset"].to_i)
        toks.pop
        toks << gid
      end
      pageLocationUtf8 = toks.join(".")
      # pageLocationUtf8 = toks[1]
    end

    if (pageLocationAscii != nil)
      begin
        h = imgDir2formats[imgDir]["head"]["title"]
      rescue
        h = imgDir.gsub("img-", "")
      end
      elmSPAN = Nokogiri::XML::Element.new("span", htmlObj)
      elmTD << elmSPAN
      elmSPAN.add_class(span_clazz)
      elmSPAN << Nokogiri::XML::Text.new(h + ":" + pageLocationUtf8, htmlObj)
      # elmSPAN << Nokogiri::XML::Text.new(pageLocationUtf8, htmlObj)
    end
  end

  elmTD = Nokogiri::XML::Element.new("td", htmlObj)
  elmTD.add_class("ucsInfo")
  elmTR << elmTD
  ucss = []
  if (Opts.include?("swucs-json"))
    (["img-Iwasaki", "img-DYC", "img-XZ-QJZ"] & imgDirs).each do |imgDir|
      bn = File::basename(imgDir2imgs[imgDir][i])

      book_key = imgDir.split("-").last.downcase
      toUcssName = imgDir.downcase + "-to-ucss"
      if (Opts[toUcssName][bn] != nil)
        newUcss = Opts[toUcssName][bn] - ucss
        if (newUcss.length > 0)
          if (elmTD.css("a").length > 0)
            elmTD << Nokogiri::XML::Text.new(",", htmlObj)
          end
          elmAUcs = Nokogiri::XML::Element.new("a", htmlObj) 
          elmTD << elmAUcs
          elmAUcs.add_class("ucs")
          elmAUcs.add_class("ucs-" + book_key)
          elmAUcs << Nokogiri::XML::Text.new(newUcss.join(","), htmlObj)
        end
        ucss << Opts[toUcssName][bn]
        ucss = ucss.flatten.compact.uniq
      end
    end
  end

  imgDirs.each_with_index do |imgDir, imgDir_idx|
    STDERR.puts [imgDir, i, imgDir2imgs[imgDir][i]].inspect
    fp = imgDir2imgs[imgDir][i]
    next if (!fp || fp.length == 0)

    imgFmts = imgDir2formats[imgDir]

    bn = File.basename(fp)
    next if (bn.length == 0)
    elmTD = Nokogiri::XML::Element.new("td", htmlObj)
    elmTR << elmTD
    elmTD.add_class("image").add_class(imgDir.gsub("/", ""))

    d = bn2imgDir[bn]
    next if (!d || !fp || (bn.length == 0) || !maxHeights[d])
    scaleF = Opts["row-height"] * 1.0 / maxHeights[d]
    scaleFF = 1.0
    if (Opts["scale" + imgDir_idx.to_s])
      scaleFF = Opts["scale" + imgDir_idx.to_s].to_f
    elsif (imgDir2formats[imgDir].include?("image") && imgDir2formats[imgDir]["image"].include?("scale"))
      scaleFF = imgDir2formats[imgDir]["image"]["scale"].to_f
    end
    if (imgFmts && imgFmts.include?("oneshot-scale") && imgFmts["oneshot-scale"].include?(bn))
      STDERR.printf("*** apply oneshot scale factor %s to %s\n", imgFmts["oneshot-scale"][bn], bn)
      scaleFF *= imgFmts["oneshot-scale"][bn].to_f
    end

    width = 0
    height = 0

    if (glyphMetrixCache.include?(fp))
      width = glyphMetrixCache[fp].width
      height = glyphMetrixCache[fp].height
    else
      ping = Magick::ImageList.new.ping(fp)
      width = ping.columns
      height = ping.rows
      glyphMetrixCache[fp] = Metrix.new.setW(width).setH(height)
      Opts["glyph-metrics-cache-has-new-info"] = true
      ping.destroy!
    end

    elmImg = Nokogiri::XML::Element.new("img", htmlObj)

    if (Opts["embed-img"])
      fd = File.open(fp)
      elmImg["src"] = "data:image/" + fp.split(".").last.downcase.gsub("jpg", "jpeg") + ";base64," + Base64::strict_encode64(fd.read)
      fd.close
    elsif (Opts["lazy-img"])
      elmImg["img-dir"] = File.dirname(fp)
    else
      elmImg["src"] = fp
    end

    elmImg["width"] = (width * scaleF * scaleFF).to_i
    elmImg["height"] = (height * scaleF * scaleFF).to_i
    # elmImg["alt"] = bn
    elmImg["title"] = bn
    elmTD << elmImg

    if (imgDir == imgDirForVolumeSep)
      # volLast = elmImg["title"].split("_").last.split(".").first
      volLast = elmImg["title"].split(/[_-]/).select{|t| t =~ /[vV][oO]?[lL]?[0-9]{2}/}.first.gsub(/^[vV][oO]?[lL]?/, "").to_i
    end
  end

  if (elmTR.css("img").length == 0)
    elmTR.add_class("no-glyph-img")
  end



  if (markArr[i + 1])
    elmTR.add_class("marked")
  elsif (Opts["hide-unmarked-rows"])
    elmTR.add_class("hidden")
  end

  if (elmTR.css("td.img-Iwasaki > img").length > 0 ||
      elmTR.css("td.img-PJG > img").length > 0 ||
      elmTR.css("td.img-CSJ > img").length > 0 || elmTR.css("td.img-CSJ2 > img").length > 0 ||
      elmTR.css("td.img-HTX > img").length > 0 || elmTR.css("td.img-HTX2 > img").length > 0)
    seqs["Xuan"] += 1
    elmA_seq["name"] = sprintf("%05d", seqs["Xuan"])
  elsif (elmTR.css("td.img-XZ-QJZ > img").length > 0)
    seqs["Kai"] += 1
    elmA_seq["name"] = sprintf("K%05d", seqs["Kai"])
  elsif (elmTR.css("td.img-Jiguge5-Kyoto > img").length > 0)
    seqs["Mao"] += 1
    elmA_seq["name"] = sprintf("M%05d", seqs["Mao"])
  elsif (elmTR.css("td.img-DYC > img").length > 0)
    seqs["Duan"] += 1
    elmA_seq["name"] = sprintf("D%05d", seqs["Duan"])
  elsif (elmTR.css("td.img-XZ-SKD > img").length > 0 || elmTR.css("td.img-XZ-WQS > img").length > 0)
    seqs["Kai_bug"] += 1
    elmA_seq["name"] = sprintf("KB%05d", seqs["Kai_bug"])
  else
    seqs["unknown"] += 1
    elmA_seq["name"] = sprintf("U%05d", seqs["unknown"])
  end
  elmA_seq << Nokogiri::XML::Text.new(elmA_seq["name"], htmlObj)

  elmTD = Nokogiri::XML::Element.new("td", htmlObj)
  elmTR << elmTD
  elmTD.add_class("comment")
  elmDIV = Nokogiri::XML::Element.new("div", htmlObj)
  elmTD << elmDIV
  elmDIV.add_class("comment")
  elmDIV << Nokogiri::XML::Text.new(ucss.join(","), htmlObj)


  char_type = Opts["xuan-index-to-type"][elmA_seq["name"]]
  case char_type
  when nil then
  when /^add/ then
    elmTR.add_class("xuan-additional")
  when /^var/ then
    elmTR.add_class("xuan-variant")
  when /^hea/ then
    elmTR.add_class("xuan-heading")
  end

  # after first TBODY > TR is added, THEAD is inserted
  if (elmTable.css("tbody > tr").length == 1)
    elmTHEAD = Nokogiri::XML::Element.new("thead", htmlObj)
    elmTable.css("tbody").first.add_previous_sibling(elmTHEAD)
    elmTR = Nokogiri::XML::Element.new("tr", htmlObj)
    elmTHEAD << elmTR

    # elmTable.css("tbody > tr").first.css("th,td").each_with_index do |elmT, i|
    elmTable.css("tbody > tr").first.children.each_with_index do |elmT, i|
      elmTH = Nokogiri::XML::Element.new("th", htmlObj)
      elmTR << elmTH

      ["seq", "glyphInfo", "ucsInfo", "comment"].each do |tc|
        elmTH.add_class(tc) if (elmT.has_class(tc))
      end

      imgDir = elmT["class"].split(/\s+/).select{|cls| imgDirs.include?(cls)}.first
      # p [i, elmT, imgDir]
      next if (imgDir == nil)

      elmTH.add_class(imgDir)
      next if (!imgDir2formats.include?(imgDir))
      next if (!imgDir2formats[imgDir].include?("head"))
      next if (!imgDir2formats[imgDir]["head"].include?("title"))
      elmTH << Nokogiri::XML::Text.new(imgDir2formats[imgDir]["head"]["title"], htmlObj)
    end
  end

  #if (elmTable.children.length >= Opts["split-per"])
  #  htmlObj, htmlIndex, = closeHtml(htmlObj, elmBody, htmlIndex, (htmlIndex > 1), (htmlIndex < 14))
  #end
  if (!Opts["no-separate-html"] && elmTBODY.children.length > 1 && (i + 1) < imgDir2imgs[imgDirForVolumeSep].length)
    nextImgForVolumeSep = imgDir2imgs[imgDirForVolumeSep][i+1].gsub(/^.*\//, "")
    if (nextImgForVolumeSep != nil && nextImgForVolumeSep.length > 0)
      # volNext = nextImgForVolumeSep.split("_").last.split(".").first
      volNext = nextImgForVolumeSep.split(/[_-]/).select{|t| t =~ /[vV][oO]?[lL]?[0-9]{2}/}.first.gsub(/^[vV][oO]?[lL]?/, "").to_i
      if (volLast != volNext)
        p ["# separate", volLast, volNext]
        htmlObj, htmlIndex, = closeHtml(htmlObj, elmBody, htmlIndex, (htmlIndex > 1), (htmlIndex < 14))
      end
    end
  end
end

if (elmTable.children.length > 0)
  htmlObj, htmlIndex, = closeHtml(htmlObj, elmBody, htmlIndex, (htmlIndex > 1), (htmlIndex < 14))
end

if (Opts["update-glyph-metrics-cache-json"] && Opts["glyph-metrics-cache-json"])
  if (!Opts["glyph-metrics-cache-has-new-info"])
    STDERR.puts("*** requested, but no need to update " + Opts["glyph-metrics-cache-json"])
  else
    STDERR.puts("*** update " + Opts["glyph-metrics-cache-json"])
    f = File.open(Opts["glyph-metrics-cache-json"], "w")
    f.write( JSON.generate(glyphMetrixCache) )
    f.close
  end
end
