var loadAJson = function(hsarr, url) {
  var jqd = $.Deferred();

  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);
  xhr.onload = function(evt) {
    if (this.readyState != 4) {
      jqd.reject();
    } else
    if (this.status == 404) {
      jqd.reject();
    } else
    if (this.status == 200 || this.status == 0) {
      hsarr.push(JSON.parse(this.responseText));
      jqd.resolve();
    }
  };
  xhr.onerror = function(evt) {
    jqd.reject();
  }
  xhr.send();

  return jqd.promise();
};

var title2base64 = null;
var loadBase64Json = function(basename_json) {
  var arr = new Array();

  var isLocal = (document.location.protocol == "file:")
  var urlPrefix = document.location.href.split("/").slice(0,-1).join("/");
  var url = basename_json;
  if (!isLocal) {
    url = urlPrefix + "/" + basename_json;
  }

  loadAJson(arr, url)
  .then(function() {
    title2base64 = arr.pop();
  });
};

//if (window.matchMedia("print")) {
//  loadBase64Json("imgdata-cache-20161120-1115.json");
//}
