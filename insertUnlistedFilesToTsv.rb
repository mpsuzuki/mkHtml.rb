#!/usr/bin/env ruby1.9

imgs = Dir.glob(ARGV.first + "/*[JGPjgp][INPinp][FGfg]").sort

lines = STDIN.readlines.collect{|l| l.chomp}

unlistedImgs = imgs.select{|img| lines.none?{|l| l.include?(img)}}

p unlistedImgs

previousListedImgs = Hash.new

unlistedImgs.each do |ul_img|
  ul_img_idx = imgs.index(ul_img)
  idx = ul_img_idx - 1
  while (lines.none?{|l| l.include?(imgs[idx])})
    idx -= 1
  end

  previousListedImgs[ul_img] = imgs[idx]
end

unlistedImgs.reverse.each do |ul_img|
  l_img = previousListedImgs[ul_img]

  l_idx = (0...(lines.length)).select{|i| lines[i].include?(l_img)}.first
  toks = lines[l_idx].split("\t")
  cell_idx = (0...(toks.length)).select{|i| toks[i].include?(ARGV.first)}.first

  if (lines[l_idx + 1].include?(ARGV.first))
    # there is no "gap" after previous listed image for this directory
    new_line = ([""] * (toks.length - 1)).insert(cell_idx, ul_img).join("\t")
    lines = lines.insert(l_idx + 1, new_line)
  else
    # there is a "gap" after previous listed image for this directory
    old_line_toks = lines[l_idx + 1].split("\t")
    old_line_toks[cell_idx] = ul_img
    lines[l_idx + 1] = old_line_toks.join("\t")
  end
end

while ($_ = lines.shift)
  puts $_
end
