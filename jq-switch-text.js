  // -------------------------------------------------------------------

  // browser dependency of Blob builder
  if (Blob) {
    window.genBlob = function(blobDataArr, blobMimeType) {
      return (new Blob(blobDataArr, {type: blobMimeType}));
    };
  } else if (window.MozBlobBuilder != undefined ) {
    window.genBlob = function(blobDataArr, blobMimeType) {
      var blobBuilder = new window.MozBlobBuilder();
      for (var i = 0; i < blobDataArr.length; i += 1) {
        blobBuilder.append(blobDataArr[i]);
      }
      return blobBuilder.getBlob(blobMimeType);
    };
  } else if (window.WebKitBlobBuilder != undefined) {
    window.genBlob = function(blobDataArr, blobMimeType) {
      var blobBuilder = new window.WebKitBlobBuilder();
      for (var i = 0; i < blobDataArr.length; i += 1) {
        blobBuilder.append(blobDataArr[i]);
      }
      return blobBuilder.getBlob(blobMimeType);
    };
  }

  // browser dependency of window.URL
  if (!window.URL && window.webkitURL) {
    window.URL = window.webkitURL;
  }


  // -------------------------------------------------------------------
  var db = {};

  var getDateTimeDigits = function(objDate) {
    var toks = new Array();
    toks.push( ("0000" + objDate.getFullYear().toString(10)).substr(-4) );
    toks.push( ("00" + (objDate.getMonth() + 1).toString(10)).substr(-2) );
    toks.push( ("00" + (objDate.getDate()).toString(10)).substr(-2) );
    toks.push( "-" );
    toks.push( ("00" + (objDate.getHours()).toString(10)).substr(-2) );
    toks.push( ("00" + (objDate.getMinutes()).toString(10)).substr(-2) );
    return toks.join("");
  }

  var updateDB = null;
  var showLinkToSaveJson = function() {
    if (updateDB) {
      updateDB();
    }
    var noteJson = JSON.stringify(db);
    var b = window.URL.createObjectURL(window.genBlob([noteJson], "text/plain"));
    var d = getDateTimeDigits(new Date()) + "_" + b.split("/").pop().split("-")[0] + ".json";

    $("a.hrefToNoteJson").prop("target", "_blank")
                         .prop("href", b)
                         .prop("download", d)
                         .click(function(){return false;})
                         .text("JSON in " + noteJson.length.toString(10) + " bytes")
                         .show();
  };

  var showLinkToLoadJson = function(jqDivMenu) {
    jqDivMenu.find("input.fileToChooseJson").show().focus().trigger("click");
  };

  var reflectDB = null;

  $("input.fileToChooseJson").change(
    function(evt){
      var fileObjs = new Array();
      for (var i = 0; i < this.files.length; i += 1) {
        fileObjs.push(this.files[i]);
      };

      var jqd = new $.Deferred();
      var loadFirstJson = function() {
        var fr = new FileReader();
        fr.onload = function(evt) {
          var tdb = JSON.parse( evt.target.result );
          for ( k in tdb ) {
            db[k] = tdb[k];
          }
          if (fileObjs.length > 0) {
            loadFirstJson();
          } else {
            jqd.resolve();
          }
        };
        fr.readAsText(fileObjs.shift(), "utf-8");
      };
      jqd.promise().then(
        function(){
          if (reflectDB) {
            reflectDB();
          }
          $("input.fileToChooseJson").hide();
          $("select.stat_io").val("nop");
        }
      );
      loadFirstJson();
    }
  );

  $("select.stat_io").change(function(evt){
                         $("a.hrefToNoteJson").hide();
                         $("input.fileToChooseJson").hide();
                         switch(this.value) {
                         case "save":
                           showLinkToSaveJson();
                           $("select.stat_io").val("nop");
                           break;
                         case "load":
                           showLinkToLoadJson($(this).parents("div.menu"));
                           break;
                         }
                      });

  // -------------------------------------------------------------------

  $("div.content").css("margin-top", $("div.navigator-top").height());

  updateDB = function() {
    $("div.comment").each( 
      function() {
        var k = $(this).parents("tr").find("th").first().text();
        var v = $(this).text();
        if (k && v) {
          db[k] = v;
        }
      }
    );
  };

  reflectDB = function() {
    $("div.comment").each(
      function() {
        var jqTR = $(this).parents("tr");
        var k = jqTR.find("th").first().text();
        if (k && db[k]) {
          $(this).empty().append(db[k]);
          if (db[k].length > 0) {
            jqTR.addClass("with-comment");
          } else {
            jqTR.removeClass("with-comment");
          }
        }
      }
    );
  };

  var textArea2TD = function(elmTA) {
    var elmTD = elmTA.parents("td.comment").first();
    var elmDIV = elmTD.children("div.comment").first();
    elmDIV.empty()
          .append(elmTA.val())
          .css("display", "block");
    elmTA.parents("form").css("display", "none");
  };

  var td2textArea = function(elmTD) {
    var elmFORM = $("form").first();
    var elmTA = elmFORM.children("textarea").first();
    var elmDIV = elmTD.children("div.comment").first();
    elmFORM.css("display", "none");
    elmTD.append(elmFORM);
    elmTA.val(elmDIV.text())
         .css("width", parseInt(elmTD.width()) - 5)
         .css("height", parseInt(elmTD.height()) - 5)
         .css("margin", 0);
    elmDIV.css("display", "none");
    elmFORM.css("margin", 0)
           .css("display", "block");
    if (elmDIV.text().length > 0) {
      $(elmTD).parents("tr").addClass("with-comment");
    } else {
      $(elmTD).parents("tr").removeClass("with-comment");
    }
  };

  $("td.comment").click(
    function(evt){
      var td = $(this);
      var ta = $("textarea").first();
      if (ta.parents("td.comment")[0] != td[0]) {
        textArea2TD(ta);
        td2textArea(td);
        ta.focus();
      }
      return false;
    }
  );

  // ---------------------------------------------------------

  var showRowsAll = function() {
    var interval = 100;
    var sizeChunk = 20;
    var trs = $("tr");
    var numTrs = trs.length;
    var cnt = numTrs;

    setTimeout(
      function() {
        var cnt0 = cnt;
        while (cnt > 0 && (cnt0 - cnt) < sizeChunk) {
          cnt -= 1;
          trs[cnt].style.removeProperty("display");
        }

        if (cnt > 0) {
          setTimeout(arguments.callee, interval);
        } else {
          // setDisplayForCssRule("tr.map-unique", "block");
        }
      },
      interval);
  }



  var hideRows = function(jqd, jqRowsToHide, cssSelectorText) {
    var interval = 100;
    var sizeChunk = 20;
    var numJqRowsToHide = jqRowsToHide.length
    var cnt = numJqRowsToHide;

    setTimeout(
      function(){
        var cnt0 = cnt;
        while (cnt > 0 && (cnt0 - cnt) < sizeChunk) {
          cnt -= 1;
          jqRowsToHide[cnt].style.display = "none";
        };

        if (cnt > 0) {
          setTimeout(arguments.callee, interval);
        } else {
          jqd.resolve();
          // setDisplayForCssRule("tr.map-unique", "none");
          // setDisplayForCssRule(cssSelectorText, "display");
        }
      },
      interval);

  };

  var hideRowsWithComments = function(jqd) {
    // hideRows(jqd, $("tbody > tr.with-comment"), "");
    var rowsToHide = new Array();
    $("tbody > tr").each(function(){
      if (this.text().replace(/\s/g, "").length > 0) {
        rowsToHide.push(this);
      }
    });
    hideRows(jqd, rowsToHide, "");
  };

  var hideRowsWithoutComments = function(jqd) {
    // hideRows(jqd, $("tbody > tr:not(.with-comment)"), "");
    var rowsToHide = new Array();
    $("tbody > tr").each(function(){
      if (this.text().replace(/\s/g, "").length == 0) {
        rowsToHide.push(this);
      }
    });
    hideRows(jqd, rowsToHide, "");
  };

  var hideRowsWithUniqueImgs = function(jqd) {
    hideRows(jqd, $("tr.map-unique"), null);
  };

  var hideForPrint = function(jqd) {
    loadAllImgs();
    $("div.navigator").hide();
    $("div.content").css({"margin-top": ""});
    $("div.link-to-prev-and-next").hide();
    jqd.resolve();
  };

  $("select.show_hide").change(function(){
                           var procSwitch = function(elm) {
                             var jqd = new $.Deferred();
                             switch(elm.value) {
                             case "showRowsAll":
                               showRowsAll(jqd);
                               break;
                             case "hideRowsWithComments":
                               hideRowsWithComments(jqd);
                               break;
                             case "hideRowsWithoutComments":
                               hideRowsWithoutComments(jqd);
                               break;
                             case "hideRowsWithUniqueImgs":
                               hideRowsWithUniqueImgs(jqd);
                               break;
                             case "hideForPrint":
                               hideForPrint(jqd);
                               break;
                             }
                             return jqd.promise();
                           };
                           var postProc = function() {
                             var jqd = new $.Deferred();
                             $(window).trigger("contentmodified");
                             $("select.show_hide").prop("disabled", false).val("nop");
                             return jqd.promise();
                           };

                           $("select.show_hide").prop("disabled", true);
                           procSwitch(this).then(postProc);
                      });

  // ---------------------------------------------------------

  $("html,body").click(
    function(evt){
      var ta = $("textarea").first();
      textArea2TD(ta);
    }
  );

  // ---------------------------------------------------------

  $(window).keydown(
    function(evt){
      if (evt.keyCode == 9) { // TAB
        var ta = $("textarea").first();
        var tr_cur = ta.parents("tr");
        if (!evt.shiftKey) {
          var tr_next = tr_cur.nextAll("tr.marked").first();
          if (tr_next && tr_next.length > 0) { 
            var td_cmt_next = tr_next.find("td.comment");
            td_cmt_next.trigger("click");
          } else {
            tr_cur.next().find("td.comment").trigger("click");
          }
        } else {
          var tr_prev = tr_cur.prevAll("tr.marked").last();
          if (tr_prev && tr_prev.length > 0) { 
            var td_cmt_prev = tr_prev.find("td.comment");
            td_cmt_prev.trigger("click");
          } else {
            tr_cur.prev().find("td.comment").trigger("click");
          }
        }
        evt.stopPropagation();
        evt.preventDefault();
      }
    }
  );
